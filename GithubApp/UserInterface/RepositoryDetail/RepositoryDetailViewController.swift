//
//  RepositoryDetailViewController.swift
//  GithubApp
//
//  Created by Damjan Miko on 20/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

import UIKit

class RepositoryDetailViewController: UIViewController {
    
    var viewModel: RepositoryDetailViewModel!
    
    convenience init(viewModel: RepositoryDetailViewModel) {
        self.init()
        self.title = UITexts.repositoryDetailTitle
        self.viewModel = viewModel
    }
    
    override func loadView() {
        buildViews()
    }
}
