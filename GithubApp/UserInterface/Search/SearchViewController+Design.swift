//
//  SearchViewController+Design.swift
//  GithubApp
//
//  Created by Damjan Miko on 20/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

import UIKit
import PureLayout

extension SearchViewController {
    
    func buildViews() {
        view = UIView()
        view.backgroundColor = UIColor.white
        
        searchTextField = UITextField()
        view.addSubview(searchTextField)
        searchTextField.autoPinEdge(toSuperviewEdge: .top, withInset: 72)
        searchTextField.autoPinEdge(toSuperviewEdge: .trailing, withInset: 10)
        searchTextField.placeholder = "Search Github"
        searchTextField.font = UIFont.systemFont(ofSize: 20)
        searchTextField.autocorrectionType = .no
        searchTextField.keyboardType = .alphabet
        
        searchImageView = UIImageView()
        let searchImage = UIImage(named: "icSearch")
        searchImageView.image = searchImage
        view.addSubview(searchImageView)
        searchImageView.autoPinEdge(toSuperviewEdge: .leading, withInset: 10)
        searchImageView.autoPinEdge(.top, to: .top, of: searchTextField)
        searchImageView.autoPinEdge(.bottom, to: .bottom, of: searchTextField)
        searchImageView.autoPinEdge(.trailing, to: .leading, of: searchTextField, withOffset: -10)
        
        bottomLine = UIView()
        view.addSubview(bottomLine)
        bottomLine.autoSetDimension(.height, toSize: 1)
        bottomLine.autoPinEdge(.top, to: .bottom, of: searchTextField, withOffset: 7)
        bottomLine.autoPinEdge(.leading, to: .leading, of: searchImageView)
        bottomLine.autoPinEdge(.trailing, to: .trailing, of: searchTextField)
        bottomLine.backgroundColor = UIColor.blue
        
        let radioButtonsStackView = UIStackView()
        view.addSubview(radioButtonsStackView)
        radioButtonsStackView.axis = .horizontal
        radioButtonsStackView.distribution = .fillEqually
        radioButtonsStackView.alignment = .center
        radioButtonsStackView.spacing = 20
        radioButtonsStackView.autoPinEdge(.top, to: .bottom, of: bottomLine, withOffset: 10)
        radioButtonsStackView.autoPinEdge(toSuperviewEdge: .leading, withInset: 10)
        radioButtonsStackView.autoPinEdge(toSuperviewEdge: .trailing, withInset: 10)
        
        starsRadioButton = RadioButton(frame: CGRect.zero, title: "Sars")
        forksRadioButton = RadioButton(frame: CGRect.zero, title: "Forks")
        updateRadioButton = RadioButton(frame: CGRect.zero, title: "Update")
        
        starsRadioButton.setTitle("Stars", for: .normal)
        starsRadioButton.setTitleColor(UIColor.black, for: .normal)
        starsRadioButton.isSelected = true
        forksRadioButton.isSelected = false
        updateRadioButton.isSelected = false
        
        radioButtonsStackView.addArrangedSubview(starsRadioButton)
        radioButtonsStackView.addArrangedSubview(forksRadioButton)
        radioButtonsStackView.addArrangedSubview(updateRadioButton)
        
        tableView = UITableView(frame: CGRect.zero, style: .plain)
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        view.addSubview(tableView)
        tableView.autoPinEdge(.top, to: .bottom, of: radioButtonsStackView, withOffset: 10)
        tableView.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .top)
    }
}
