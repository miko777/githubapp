//
//  SearchViewModel.swift
//  GithubApp
//
//  Created by Damjan Miko on 20/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

import RxSwift

class SearchViewModel {
    
    var searchResults: Observable<[RepositorySection]>!
    let searchText = Variable<String>("")
    var sort = Variable<Sort>(Sort.forks)
    
    var disposeBag = DisposeBag()
    
    init(dependecies: AppDependecies) {
        
        let searchTextObservable = searchText.asObservable()
            .throttle(0.5, scheduler: MainScheduler.instance)
            .distinctUntilChanged()

        let sortObservable = sort.asObservable().map({$0.rawValue})
        
        
        searchResults = Observable.merge(searchTextObservable, sortObservable)
            .flatMap( { [weak self] _ -> Observable<[RepositorySection]> in
                guard let `self` = self else { return Observable.just([]) }
                if self.searchText.value.isEmpty {
                    return Observable.just([])
                }
                return dependecies
                    .searchGithubService
                    .searchRepositories(query: self.searchText.value, sortMethod: self.sort.value)
                    .map { [RepositorySection(header: "", items:$0)] }
        }).observeOn(MainScheduler.instance)
    }
}
