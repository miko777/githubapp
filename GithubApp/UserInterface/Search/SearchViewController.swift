//
//  SearchViewController.swift
//  GithubApp
//
//  Created by Damjan Miko on 20/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources

class SearchViewController: UIViewController {
    
    private let repositoryTableViewCellIdentifier = String(describing: RepositoryViewCell.self)
    
    var searchTextField: UITextField!
    var bottomLine: UIView!
    var searchImageView: UIImageView!
    var tableView: UITableView!
    var starsRadioButton: RadioButton!
    var forksRadioButton: RadioButton!
    var updateRadioButton: RadioButton!
    
    var viewModel: SearchViewModel!
    var disposeBag = DisposeBag()
    
    convenience init(viewModel: SearchViewModel) {
        self.init()
        self.title = UITexts.searchTitle
        self.viewModel = viewModel
    }
    
    override func loadView() {
        buildViews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(RepositoryViewCell.self, forCellReuseIdentifier: repositoryTableViewCellIdentifier)
        
        let dataSource = RxTableViewSectionedReloadDataSource<RepositorySection>(
            configureCell: { ds, tv, ip, item in
                let cell = tv.dequeueReusableCell(withIdentifier: self.repositoryTableViewCellIdentifier) as! RepositoryViewCell
                cell.setup(repository: item)
                tv.rowHeight = CGFloat(cell.cellHeight)
                return cell
        })
        
        tableView
            .rx
            .itemSelected
            .subscribe(onNext: { [weak self] _ in
                guard let `self` = self else { return }
                
                if let selectedRow = self.tableView.indexPathForSelectedRow {
                    self.tableView.deselectRow(at: selectedRow, animated: false)
                }
            })
            .disposed(by: disposeBag)
        
        searchTextField.rx.text.orEmpty
            .distinctUntilChanged()
            .skip(1)
            .bind(to: viewModel.searchText)
            .disposed(by: disposeBag)
        
        viewModel.searchResults?
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        starsRadioButton.alternateButton = [forksRadioButton, updateRadioButton]
        forksRadioButton.alternateButton = [starsRadioButton, updateRadioButton]
        updateRadioButton.alternateButton = [forksRadioButton, starsRadioButton]
        
        starsRadioButton.rx.tap
            .map({Sort.stars})
            .bind(to: viewModel.sort)
            .disposed(by: disposeBag)
            
        forksRadioButton.rx.tap
            .map({Sort.forks})
            .bind(to: viewModel.sort)
            .disposed(by: disposeBag)
            
        updateRadioButton.rx.tap
            .map({Sort.updated})
            .bind(to: viewModel.sort)
            .disposed(by: disposeBag)
    }
}
