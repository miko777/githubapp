//
//  RepositoryViewCell.swift
//  GithubApp
//
//  Created by Damjan Miko on 20/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

import UIKit

class RepositoryViewCell: UITableViewCell {
    
    var cellHeight: CGFloat = 100
    
    var repositoryNameLabel: UILabel!
    var ownerNameLabel: UILabel!
    var watchersLabel: UILabel!
    var forksLabel: UILabel!
    var openIssuesLabel: UILabel!
    var authorAvatarImageView: UIImageView!
    

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        buildViews()
    }
    
    func setup(repository: Repository) {
        
        let url = URL(string: repository.owner.avatarUrl)
        let data = try? Data(contentsOf: url!)
        authorAvatarImageView.image = UIImage(data: data!)
        
        print(repository.name)
    }
}
