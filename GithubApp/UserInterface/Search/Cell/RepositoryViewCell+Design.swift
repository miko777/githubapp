//
//  RepositoryViewCell+Design.swift
//  GithubApp
//
//  Created by Damjan Miko on 20/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

import UIKit

extension RepositoryViewCell {
    
    func buildViews() {
        
        authorAvatarImageView = UIImageView()
        contentView.addSubview(authorAvatarImageView)
        authorAvatarImageView.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 0), excludingEdge: .trailing)
        authorAvatarImageView.autoMatch(.height, to: .width, of: authorAvatarImageView)
        
    }
}
