//
//  UserDetailViewController.swift
//  GithubApp
//
//  Created by Damjan Miko on 20/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

import UIKit

class UserDetailViewController: UIViewController {
    
    var viewModel: UserDetailViewModel!
    
    convenience init(viewModel: UserDetailViewModel) {
        self.init()
        self.title = UITexts.userDetailTitle
        self.viewModel = viewModel
    }
    
    override func loadView() {
        buildViews()
    }
}

