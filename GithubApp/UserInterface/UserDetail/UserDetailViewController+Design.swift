//
//  UserDetailViewController+Design.swift
//  GithubApp
//
//  Created by Damjan Miko on 20/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

import UIKit

extension UserDetailViewController {
    
    func buildViews() {
        view = UIView()
        view.backgroundColor = UIColor.white
    }
}
