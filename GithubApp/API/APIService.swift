//
//  SearchGithubService.swift
//  GithubApp
//
//  Created by Damjan Miko on 20/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import ObjectMapper

class APIService {
    
    private let sessionService: URLSession
    
    init() {
        sessionService = URLSession.shared
    }
    
    func searchRepositories(query: String, sortMethod: Sort) -> Observable<[Repository]> {
        let urlString = String(format: "%@%@%@%@",
                               APIConstants.baseUrl,
                               APIConstants.searchRepositoryEndpoint,
                               APIConstants.repositoryQuery(for: query),
                               APIConstants.sortQuery(for: sortMethod))
        let url = URL(string: urlString)!
        let request = URLRequest.createGetRequest(with: url)
        
        return sessionService.rx
            .response(request: request)
            .map { (response, data) -> [Repository] in
                guard let stringData = String(data: data, encoding: .utf8),
                    let result = Result(JSONString: stringData)
                    else { throw APIParsingError.parsingJsonError }
                return result.items
                
            }
    }
}

