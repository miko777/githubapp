//
//  APIConstants.swift
//  GithubApp
//
//  Created by Damjan Miko on 20/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

struct APIConstants {
    
    static let baseUrl = "https://api.github.com"
    static let searchRepositoryEndpoint = "/search/repositories"
    
    static func repositoryQuery(for query: String) -> String {
        return "?q=\(query)"
    }
    
    static func sortQuery(for sortMethod: Sort) -> String {
        return "&sort=\(sortMethod.rawValue)"
    }
}

struct JSONKey {
    static let items = "items"
    
    static let repositoryName = "name"
    static let owner = "owner"
    static let watchers = "watchers"
    static let forks = "forks"
    static let openIssues = "open_issues"
    static let score = "score"
    
    static let userName = "login"
    static let avatarUrl = "avatar_url"
    
}

enum APIParsingError: Error {
    case parsingJsonError
}

enum Sort: String {
    case stars = "stars"
    case forks = "forks"
    case updated = "updated"
}
