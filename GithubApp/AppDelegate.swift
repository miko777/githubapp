//
//  AppDelegate.swift
//  GithubApp
//
//  Created by Damjan Miko on 20/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appCoordinator: AppCoordinator!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        initialSetup()
        return true
    }
    
    private func initialSetup() {
        window = UIWindow(frame: UIScreen.main.bounds)
        appCoordinator = AppCoordinator(window: window!)
    }

}

