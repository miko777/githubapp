//
//  AppCoordinator.swift
//  GithubApp
//
//  Created by Damjan Miko on 20/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

import UIKit

class AppCoordinator {
    
    private var window: UIWindow!
    private var appDependecies: AppDependecies
    
    init(window: UIWindow) {
        self.window = window
        self.appDependecies = AppDependecies()
        presentInitialScreen()
    }
    
    public func presentInitialScreen() {
        let searchViewModel = SearchViewModel(dependecies: appDependecies)
        let searchViewController = SearchViewController(viewModel: searchViewModel)
        window.rootViewController = UINavigationController(rootViewController: searchViewController)
        window.makeKeyAndVisible()
    }
    
}
