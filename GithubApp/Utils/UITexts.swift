//
//  UITexts.swift
//  GithubApp
//
//  Created by Damjan Miko on 20/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

import Foundation

class UITexts: NSObject {
    
    static let searchTitle = "Search Github"
    static let repositoryDetailTitle = "Repository Detail"
    static let userDetailTitle = "User Detail"
    
}
