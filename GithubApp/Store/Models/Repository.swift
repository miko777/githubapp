//
//  Repository.swift
//  GithubApp
//
//  Created by Damjan Miko on 20/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

import ObjectMapper
import RxDataSources

class Result: Mappable {
    var items: [Repository]!
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        items <- map[JSONKey.items]
    }
}

class Repository: Mappable {
    var name: String!
    var owner: User!
    var watchers: Int!
    var forks: Int!
    var openIssues: Int!
    var score: Double!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map[JSONKey.repositoryName]
        owner <- map[JSONKey.owner]
        watchers <- map[JSONKey.watchers]
        forks <- map[JSONKey.forks]
        openIssues <- map[JSONKey.openIssues]
        score <- map[JSONKey.score]
    }
}

struct RepositorySection {
    var header: String
    var items: [Repository]
}

extension RepositorySection : SectionModelType {
    var identity: String {
        return header
    }
    
    init(original: RepositorySection, items: [Repository]) {
        self = original
        self.items = items
    }
}
