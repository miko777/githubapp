//
//  User.swift
//  GithubApp
//
//  Created by Damjan Miko on 20/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

import ObjectMapper

class User: Mappable {
    var name: String!
    var avatarUrl: String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map[JSONKey.userName]
        avatarUrl <- map[JSONKey.avatarUrl]
    }
    
}
